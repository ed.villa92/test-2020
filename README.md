# Ambiente de desarrollo
## Prepara tu ambiente

Descarga el repo.  

Ejecuta el back con los siguiente comandos:

**crear ambiente virtual**
```
virtualenv env/
```

**activar**
```
source env/bin/activate
```

**instalar**
```
pip install -r requirements.txt
```

**crear base de datos**
```
python manage.py migrate
```

**correr la aplicación**
```
cd src/
python manage.py runserver
```

# 1. Backend

## 1.1) crear un usuario con la siguiente información:

- `user`
- `email`
- `first_name`
- `last_name`
- `role`

## 1.2) usa el método `create_user` disponible en views.py, para guardar un nuevo usuario en la base de datos y regresa la información ingresada más el pk:
- `pk`
- `user`
- `password`
- `email`
- `first_name`
- `last_name`
- `role` Con dos tipos (ADMIN, BASE)

## 1.3) si el usuario ya existe regresa el usuario existente

## Notas:
- Puedes usar librerías adicionales

## Bonus points:
### Dominating:
- Implementa un método GET para obtener el usuario a través de un id.

### Rampage:
- Valida y restringue el método GET solo a usuarios con los tipos de roles definidos.
- Retorna un código de error HTTP 204 en caso de que no exista el usuario.

### Killing Spree:
- Usa django rest framework para crear los views y regresar una respuesta.
- Usa "users/" como url para estas nueva(s) vista(s).
- No sobreescribas y/o elimines la información.

### Monster kill
- Recibe y devuelve la información en formato JSON
- Envía la información con encabezado Content-Type: application/json

### Unstoppable
- Usa uno o más serializadores para validar la información.

### Ultra kill
- Recibe y devuelve la información en camelCase. Por ejemplo: "firstName" en vez de "first_name"

### Godlike
- Autentica usando JWT

### Wicked sick
- Restringe POST solo a usuarios con role `ADMIN` definido previamente en tus modelos usando permisos.

### Holy Moly Wacamoly!
- Valida el funcionamiento con unit tests.


# 2. Frontend (Extra)

## 2.1) Desarrolla la interfaz usando vue que permita crear un usuario con la siguiente información:
- `user`
- `password`
- `email`
- `first_name`
- `last_name`
- `role`

## 1.2) Valida que `email` sea un correo electrónico válido